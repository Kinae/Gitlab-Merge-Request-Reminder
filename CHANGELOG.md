# [1.4.0](https://github.com/BottlecapDave/Gitlab-Merge-Request-Reminder/compare/v1.3.0...v1.4.0) (2022-02-19)


### Features

* Added support for custom gitlab domain ([ac6f909](https://github.com/BottlecapDave/Gitlab-Merge-Request-Reminder/commit/ac6f9099657c74284c9c94a81b138eb1e7f1d77e))

# [1.3.0](https://github.com/BottlecapDave/Gitlab-Merge-Request-Reminder/compare/v1.2.2...v1.3.0) (2021-12-22)


### Features

* use non-zero exit code on error ([a5e8680](https://github.com/BottlecapDave/Gitlab-Merge-Request-Reminder/commit/a5e8680e4851fa1650a5a117ca368b507d1c46ac))
